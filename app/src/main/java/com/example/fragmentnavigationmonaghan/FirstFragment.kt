package com.example.fragmentnavigationmonaghan

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.fragmentnavigationmonaghan.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonGreen.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_greenFragment)
        }
        binding.buttonRed.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_redFragment2)
        }
        binding.buttonBlue.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_blueFragment)
        }
        binding.buttonGray.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_grayFragment)
        }
        binding.buttonYellow.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_yellowFragment)
        }
        binding.buttonPurple.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_purpleFragment)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}